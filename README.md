**Some Basic Command lines to operate GITLAB**
 
- To add all file<br />
$ git add .


- To commit with msg<br />
$ git commit -m "the message"


- To push<br />
$ git push origin branch_name


- To pull<br />
 $ git pull origin branch_name<br />
 When update with the master, branch_name = master


- To make a new branch<br />
 $ git checkout -b "branch_name"


- To switch branches<br />
 $ git checkout "branch_name"


- To know the status<br />
 $ git status<br />
 add => ready to be stages (commited) commit => local repo => update push => remote repo update


- To Check previous commit<br />
 $ git log --oneline


- To reset to previous commit<br />
 $ git reset --hard <Commit ID>


- To switch Branches<br />
 $ git checkout <Branch_name><br />
 Use master to goto master


**Steps to work on the project**

1. Clone your project<br />
  $ git clone urlofproject

2. Open project in unreal and make edits. 

3. Do whatever work you want to do, then when you finish, write the     following commands in gitbash (make sure you are in your own branch).<br />
  $ git checkout "branch_name"<br />
  $ git add .<br />
  $ git commit -m "changes that have been made"<br />
  $ git push origin branch_name<br />

4. If some one wants to work on other’s branch<br />
  Just pull the project from the branch using<br />
  $ git checkout "branch_name"<br />
  $ git pull origin branch_name

5. Its important after the initialization always pull the project from the  desired the branch before making any local changes.

6. Only the necessary assets from the content packs otherwise it will be too space consuming.

